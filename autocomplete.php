<?php
if (isset($_POST['search_term']) && !empty($_POST['search_term'])){
    include_once "dbconfig.php";
    $conn = DatabaseConnection::getConnection();
    $q = "SELECT organisatienaam FROM `organisatie` WHERE `organisatienaam` LIKE '%$search%' ORDER BY organisatie ASC";
    $stmt = $conn->prepare($q);
    $stmt->execute();

    while ($row = $stmt->fetch()) {
       return $row["organisatienaam"];
    }
}
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<div class="input-group">
    <input type="text" class="form-control autosuggest" placeholder="Search for">
    <span class="input-group-btn">
        <button class="btn btn-info">zoek</button>
    </span>
</div>
<div class="dropdown">
    <ul class="result">
    </ul>
</div>

<script>
    $(document).ready(function() {

$(".autosuggest").keyup(function() {

    let search_term  = $(this).attr('search');
    let dataString = 'search_term='+ search_term;
    $.ajax({
        type: 'post',
        url : '',
        data: dataString,
        success: function(data) {
            alert(data);
        }
    });
});

});
    </script>