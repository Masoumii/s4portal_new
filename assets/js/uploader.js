function _(el){
    return document.getElementById(el);
}

function uploadFile() {

    // Only if user has selected a file, Start upload progess
    if (document.getElementById("docfile").value != "") {

        let file = _("docfile").files[0]; // File
        let orgId = $("#organisation").val(); // Org ID

        let formdata = new FormData();
        formdata.append("docfile", file); // File
        formdata.append("organisation", orgId); // Organisation

        /* Upload file with AJAX */
        let ajax = new XMLHttpRequest(); // Instantiate XML class
        ajax.upload.addEventListener("progress", progressHandler, false);
        ajax.addEventListener("load", completeHandler, false);
        ajax.addEventListener("error", errorHandler, false);
        ajax.addEventListener("abort", abortHandler, false);

        ajax.open("POST", "file_upload_parser.php"); // POST to PHP
        ajax.send(formdata); // Send form 
    }
}

/* PROGRESS HANDLER */
function progressHandler(event){

    /* Calculate uploaded percentage */
    let percent = Math.round((event.loaded / event.total) * 100);
    console.log(percent + "%");

    /* Change text button */
    $("#hide-btn").html(percent+"%");

    /* disable upload button */
    $("#hide-btn").prop("disabled", true);

    /* Increment the progressbar */
    $('#uploadProg').attr('aria-valuenow', percent).css('width', percent + "%");

    /* If 100% reached, then show notification that upload is wrapping up */
    if (Math.round(percent) === 100) {
        $("#hide-btn").html("Upload aan het afronden...");
    }

}

/* COMPLETE HANDLER */
function completeHandler(event){
   
    /* Change upload button text */
    $("#hide-btn").html("Klaar met uploaden!");

    /* Reload page */
    setTimeout(function(){
        window.location.reload(true); 
        }, 500);
}

/* ERROR HANDLER */
function errorHandler(event){
    // Show notification if upload failed
    //$(".fixed-nav-bar-bottom").val("Uploaden is niet gelukt.. .");
    alert("Uploaden is niet gelukt...");
    $("#hide-btn").val("Uploaden is niet gelukt...");
}
// Show notification if upload canceled
function abortHandler(event){
    $("#hide-btn").val("Uploaden is geannuleerd");
}