<?php

/* If submit button pressed down */
if (isset($_POST['btnSubmit'])) {

    /* Error array */
    $errors= array();

    /*  Uploaded file parameters */
    $uploadfileTempName = $_FILES["uploadImage"]["tmp_name"];  // Temp file name
    $uploadfileName     = $_FILES['uploadImage']['name'];      // file name
    $uploadfileSize     = $_FILES["uploadImage"]["size"];      // file size
    $uploadfileType     = $_FILES["uploadImage"]["type"];      // file type

    /* Get the file extension */
    $file_ext=strtolower(end(explode('.',$uploadfileName)));

    /* Array with allowed extensions */
    $extensions= array("pdf","doc","docx","xls","xlxs","txt");

    /* Check if file extension is allowed */
    if(in_array($file_ext, $extensions) === false){
        $errors[]="Gekozen bestandstype is niet toegestaan.";
     }

     /* Check if file size is allowed */
     if($uploadfileSize > 10097152){
        $errors[]='Bestand is te groot. ( max 10 mb )';
     }

    /* Upload Folder */
    $folderPath = "uploads/";
    
    /* Check if folder path is writable or not */
    if (! is_writable($folderPath) || ! is_dir($folderPath)) {
        echo "error";
        exit();
    }

    /* If succesful */
    if(empty($errors)==true){
    if (move_uploaded_file($uploadfileTempName, $folderPath .$uploadfileName)) {
        echo 'File succesfully uploaded'. $folderPath . "" . $uploadfileName;
        exit();
    }
}else{
    print_r($errors);
 }

}
