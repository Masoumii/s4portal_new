$(function(){

    /* Update interval in seconds */
    const UPDATEINTERVAL = 10;
    /* Update notifications on page load */
    updateNotifications();

    /* Mark as read on-click binder function */
    $('.mark-as-read').on("click", function(){
        updateNotifications();
        if ($(".dropdown-menu.show").is(":visible")) {
            markAsRead();
            updateNotifications();
          }else{
              updateNotifications();
          }
    })

    /* Mark as read all notifications function */
    function markAsRead(){

        $.ajax({
            type: 'POST',
            url: 'notification_system/notification.controller.php',
            data: { markAsRead:"markAsRead"},

            beforeSend:function(){
                console.log("Marking as read...");
               },
   
            success:function(data){
                // successful request; do something with the data
                console.log("Marking as read success!");
               },
   
            error:function(){
                // failed request; give feedback to user
                $('.n-list').html('<p class="error"><strong>Oops!</strong> Could not mark as read.</p>');
               }

          });

    }

    /* Update all notifications function */
    function updateNotifications(){

        // Get all notifications
        $.ajax({
            type: 'POST',
            url: 'notification_system/notification.controller.php',
            data: { updateNotif:"updateNotif"},

            beforeSend:function(){
                console.log("Checking for new data...");
                },
   
            success:function(data){

                let lastData = $('.notify-list').html(); // Get the last data
                let currentData = data;                  // Get the new data

                // Compare the last data with the new data, if data changed , update the notify list
                if(lastData !== currentData){
                     $('.notify-list').html(currentData);
                     console.log("New data appended");
                }else{
                    console.log("No new data appended");
                }
                   
                },
   
            error:function(){
                // failed request; give feedback to user
                $('.n-list').html('<p class="error"><strong>Oops!</strong> Could not update your notifications.</p>');
                }
          });


          // Recount all the notifications
          $.ajax({
            type: 'POST',
            url: 'notification_system/notification.controller.php',
            data: { countNotif:"countNotif"},

            beforeSend:function(){
                //console.log("Counting notifications...");
               },
   
            success:function(data){

                let lastData = $('.n-count').html(); // Get the last data
                let currentData = data;              // Get the new data

                // Compare the last data with the new data, if data changed , update the notify list
                if(lastData !== currentData){

                    /* If notification is count higher than zero */
                    if(data !== "0"){
                        $('.notification-area li .ti-bell>span').css({"background-color":"#F44336"});
                        $('.notification-area li .ti-bell>span, .n-count').html(data);
                    /* If notification count is zero */
                    }else{
                        $('.notification-area li .ti-bell>span').css({"background-color":"#08475b"});
                        $('.notification-area li .ti-bell>span, .n-count').html(data);
                    } 
                }
      
               },
   
            error:function(){
                // failed request; give feedback to user
                $('.n-list').html('<p class="error"><strong>Oops!</strong> Could not Count your notifications.</p>');
               }
          });
    }

    /* Update notifications periodically (every 5 seconds)*/
        setInterval(function (){
              updateNotifications();
          }, UPDATEINTERVAL*1000);

})