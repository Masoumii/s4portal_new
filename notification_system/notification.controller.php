<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if(!isset($_SESSION)){session_start();}

/* Mark all notifications as read */
if(isset($_POST['markAsRead'])){
    $userId = $_SESSION['user_id'];
    require_once "notification.class.php";
    $Notification = new Notification();
    $Notification->markAsRead($userId);
}
/* Update(get) all notifications */
if(isset($_POST['updateNotif'])){
    $userId =  $_SESSION['user_id'];
    $userRol = $_SESSION['userRol'];
    require_once "notification.class.php";
    $Notification = new Notification();
    $Notification->getAllNotifications($userId, $userRol);
}
/* Count all notifications */
if(isset($_POST['countNotif'])){
    $userId = $_SESSION['user_id'];
    require_once "notification.class.php";
    $Notification = new Notification();
    $Notification->countAllNotifications($userId);
}