<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}

/* If user is not logged in, redirect him back to login page */
if ($_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

/* Initialise the class and assign variable to use it on the page */
require "classes/s4portal.class.php";
$S4Portal = new S4Portal();
?>

<!doctype html>
<!-- no-js until js is attached to it -->
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <!-- IE -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- Tab name -->
    <title>S4Portal</title>
    <!-- Top headers -->
    <?php require "headers.php"; ?>
</head>

<body>
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->

    <!-- page container area start -->
    <div class="page-container">

        <!-- sidebar menu area start -->
        <?php $S4Portal->renderMenu(); ?>
        <!-- sidebar menu area end -->

        <!-- main content area start -->
        <div class="main-content">

            <!-- header area start -->
            <div class="header-area">

                <div class="row align-items-center">

                    <!-- nav and search button start -->
                    <?php $S4Portal->renderNavAndSearch(); ?>
                    <!-- nav and search button end -->

                    <!-- profile info & task notification start -->
                    <?php $S4Portal->renderProfileAndNotifications(); ?>
                    <!-- profile info & task notification end -->

                </div>
            </div>
            <!-- header area end -->

            <!-- page title area start -->
            <?php $S4Portal->renderPageTitle("Organisaties"); ?>
            <!-- page title area end -->

            <div class="main-content-inner">
                <br>
                <!-- row area start -->
                <div class="row-fluid">


                    <!-- Modal -->
                    <form id="editOrgDetails" action="" method="POST">
                        <div class="modal fade" id="orgModal" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title noselect"><i class="ti-pencil-alt"></i>&nbsp;&nbsp;Organisatiegegevens wijzigen</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                                    </div>

                                    <div class="modal-body">

                                        <!-- Organisatie ID (Hidden)-->
                                        <input class="editOrgId" type="hidden" name="orgId" value="">

                                        <!-- Organisatienummer -->
                                        <label for="editOrgNum">Organisatienummer</label>
                                        <input name="orgNum" id="editOrgNum" class="editOrgNum form-control col-sm-12" type="text">
                                        <br>

                                        <!-- Organisatienaam -->
                                        <label for="editOrgName">Organisatienaam</label>
                                        <input name="orgName" id="editOrgName" type="text" class="editOrgName form-control col-sm-12" required>
                                        <br>

                                        <!-- Adres -->
                                        <label for="editOrgAddress">Adres</label>
                                        <input name="orgAddress" id="editOrgAddress" type="text" class="editOrgAddress form-control col-sm-12" required>
                                        <br>

                                        <!-- Huisnummer -->
                                        <label for="editOrgHousenr">Huisnummer</label>
                                        <input name="orgHousenr" id="editOrgHousenr" type="text" class="editOrgHousenr form-control col-sm-12" required>
                                        <br>

                                        <!-- Postcode -->
                                        <label for="editOrgPostal">Postcode</label>
                                        <input name="orgPostal" id="editOrgPostal" type="text" class="editOrgPostal form-control col-sm-12" required>
                                        <br>

                                        <!-- Plaats  -->
                                        <label for="editOrgCity">Plaats</label>
                                        <input name="orgCity" id="editOrgCity" type="text" class="editOrgCity form-control col-sm-12" required>
                                        <br>

                                        <!-- Land -->
                                        <label for="editOrgCountry">Land</label>
                                        <select name="orgCountry" class="editRol form-control" id="editOrgCountry" required>
                                            <?= $S4Portal->getAllCountryOptions() ?>
                                        </select>
                                        <br>

                                        <!-- Tel -->
                                        <label for="editOrgPhone">Telefoonnummer</label>
                                        <input name="orgPhone" id="editOrgPhone" type="text" class="editOrgPhone form-control col-sm-12" required>
                                        <br>

                                        <!-- Email  -->
                                        <label for="editOrgEmail">Emailadres</label>
                                        <input name="orgEmail" id="editOrgEmail" type="email" class="editOrgEmail form-control col-sm-12" required>
                                        <br>

                                        <!-- Status  -->
                                        <label for="editOrgStatus">Status</label>
                                        <select name="orgStatus" class="editOrgStatus form-control" id="editOrgStatus" required>
                                            <?= $S4Portal->getAllStatusOptions() ?>
                                        </select>
                                        <br>

                                        <!--ERP Systeem -->
                                        <label for="editOrgERP">ERP Systeem</label>
                                        <input name="orgERP" class="editOrgERP form-control" id="editOrgERP">
                                        <br>

                                        <!--CMS Systeem -->
                                        <label for="editOrgCMS">ERP Systeem</label>
                                        <input name="orgCMS" class="editOrgCMS form-control" id="editOrgCMS">
                                        <br>

                                        <!--Kredietverzekeraar -->
                                        <label for="editOrgIns">Kredietverzekeraar</label>
                                        <input name="orgIns" class="editOrgIns form-control" id="editOrgIns">
                                        <br>

                                        <!--Informatiebureau -->
                                        <label for="editOrgInf">Informatiebureau</label>
                                        <input name="orgInf" class="editOrgInf form-control" id="editOrgInf">
                                        <br>

                                        <!--Incassopartner -->
                                        <label for="editOrgIncasso">Incassopartner</label>
                                        <input name="orgIncasso" class="editOrgIncasso form-control" id="editOrgIncasso">
                                        <br>

                                        <!-- S4 Product -->
                                        <label for="editOrgS4Prod">S4 Product</label>
                                        <select class="S4ProdDatalist" name="orgS4Prod" class="editOrgS4Prod form-control hidden" id="editOrgS4Prod">
                                            <?= $S4Portal->getAllS4ProductsOptions() ?>
                                        </select>
                                        <br><br>

                                        <!-- Relatiebeheerder -->
                                        <label for="editOrgRel">Relatiebeheerder</label>
                                        <select id="editOrgRel" name="orgRel" class="editOrgRel form-control" required>
                                            <?= $S4Portal->getAllRelatiebeheerderOptions() ?>
                                        </select><br>

                                        <!-- Notities -->
                                        <label for="editOrgNotes">Notities</label>
                                        <textarea name="orgNotes" class="editOrgNotes form-control" id="editOrgNotes"></textarea>
                                        <br>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                        <button type="submit" name="saveOrgChanges" class="btn btn-primary">Wijziging opslaan</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="col-lg-12">
                        <div class="row">

                            <!-- Begin Charts (left side) -->
                            <div class="col-md-4 mt-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                            <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                            </div>
                                            <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                            </div>
                                        </div>
                                        <h4 class="header-title noselect" style="color:#08475b"><i class="ti-organisations"></i>&nbsp;Soorten klanten</h4>
                                        <canvas id="customerTypesChart" height="349" width="450" class="chartjs-render-monitor" style="display: block;"></canvas>
                                    </div>
                                </div>
                            </div>
                            <!-- End Charts (right side) -->

                            <!-- Table: Alle organisaties -->
                            <div class="col-md-8 mt-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="header-title noselect" style="color:#08475b"><i class="ti-organisations"></i>&nbsp;Alle organisaties</h4>
                                        <div class="single-table">
                                            <div class="table-responsive">
                                                <!-- Table start -->
                                                <table id="organisations" name="organisations" class="table table-hover progress-table text-center display">
                                                    <!-- Table headers -->
                                                    <thead class="text-uppercase">
                                                        <tr>
                                                            <th scope="col">Status</th>
                                                            <th scope="col">Naam</th>
                                                            <th scope="col">Contactpersoon</th>
                                                            <th scope="col">Aangemaakt op</th>
                                                            <th scope="col">acties</th>
                                                        </tr>
                                                    </thead>
                                                    <!-- Table body/rows -->
                                                    <tbody>
                                                        <?= $S4Portal->renderOrganisationTable() ?>
                                                    </tbody>
                                                </table>
                                                <!-- Table end -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                            <br><br>
                                <div class="card">
                                    <div class="card-body">
                                        <!-- Start accordion menu -->
                                        <div id="accordion2" class="according accordion-s2">
                                            <!-- TAB 1: 'Gebruiker aanmaken' tab inside Accordion menu -->
                                            <!-- Gebruiker aanmaken tab -->
                                            <div class="card">
                                                <div class="card-header">
                                                    <a style="color:#08475b" class="card-link collapsed" data-toggle="collapse" href="#accordion21" aria-expanded="false"><i class="ti-add"></i>&nbsp;&nbsp;Nieuwe organisatie</a>
                                                </div>
                                                <div id="accordion21" class="collapse" data-parent="#accordion2" style="">
                                                    <div class="card-body">
                                                        <div class="row">
                                                                                    <!-- Create new organisation -->
                                                        <div class="col-6 mt-3">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <?= $S4Portal->renderOrganisationCreationView() ?>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <br>
                                                        </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End accordion menu -->
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <!-- main content area end -->

                    <!-- footer area start-->
                    <?php $S4Portal->renderFooter(); ?>
                    <!-- footer area end-->
                </div>

                <!-- Bottom headers required on all pages -->
                <?php require "headersBottom.php"; ?>
</body>

</html>