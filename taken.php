<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}

/* If user is not logged in, redirect him back to login page */
if ($_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}

/* Initialise the class and assign variable to use it below */
require "classes/s4portal.class.php";
$S4Portal = new S4Portal();

/* If user is not an S4Portal Administrator, redirect user back to index / hide this page */
//$S4Portal->restrictAccess();

?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>S4Portal</title>
    <?php require "headers.php"; ?>
</head>

<body>

    <!-- preloader area start -->
    <?php $S4Portal->renderPreloader(); ?>
    <!-- preloader area end -->

    <!-- page container area start -->
    <div class="page-container">

        <!-- Render the menu -->
        <?php $S4Portal->renderMenu(); ?>

        <!-- main content area start -->
        <div class="main-content">

            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">

                    <!-- nav and search button start -->
                    <?php $S4Portal->renderNavAndSearch(); ?>
                    <!-- nav and search button end -->

                    <!-- profile info & task notification start -->
                    <?php $S4Portal->renderProfileAndNotifications(); ?>
                    <!-- profile info & task notification end -->

                </div>
            </div>

            <!-- header area end -->

            <!-- page title area start -->
            <?php $S4Portal->renderPageTitle("Taken"); ?>
            <!-- page title area end -->

            <div class="main-content-inner">

                         <!-- Modal : Assign task --> 
                         <!-- Modal -->
                        <form id="assignTask" action="controller.php" method="POST">
                        <div class="modal fade" id="tasksModal" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title noselect"><i class="ti-pencil-alt"></i>&nbsp;&nbsp;Taak laten opvolgen</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                                    </div>

                                    <div class="modal-body">

                                        <!-- Taak ID (ID van oorspronkelijke taak waarin deze wordt opgevolgd, Hidden)-->
                                        <input class="originalTaskId" type="hidden" name="originalTaskId" value="">

                                        <!-- Taak (content) -->
                                        <label for="taskContent">Taak omschrijving</label>
                                        <input id="taskContent" class="taskContent form-control" type="text" name="taskContent" value=""><br>

                                        <!-- Organisatie ID (Hidden)-->
                                        <input class="taskOrgId" type="hidden" name="taskOrgId" value="">

                                         <!-- Taak rol -->
                                         <input class="taskRolId" type="hidden" name="taskRolId" value="<?=$_SESSION['userRol']?>">

                                        <!-- Task creation date -->
                                        <input class="taskCreationDate" name="taskCreationDate" type="hidden" value="">

                                        <!-- Task owner ID -->
                                        <input class="taskCreatorId" name="taskCreatorId" type="hidden" value="<?=$_SESSION['user_id']?>">
                                        
                                        <!-- Actiehouder -->
                                        <label for="taskFollowupHolderId">Actiehouder</label>
                                        <select name="taskFollowupHolderId" class="taskFollowupHolderId form-control" id="taskFollowupHolderId" required>
                                            <?= $S4Portal->getAllTaskUsers()?>
                                        </select>
                                        <br>

                                        <!-- Opvolgactie --> 
                                        <label for="taskFollowupActionId">Opvolgactie</label>
                                        <select name="taskFollowupActionId" class="taskLabel form-control" id="taskFollowupActionId" required>
                                            <?=$S4Portal->getAllTaskActions()?>
                                        </select>
                                        <br>

                                        <!-- Opvolgdatum -->
                                        <label for="taskFollowupDate">Opvolgdatum</label>
                                        <input type="date" name="taskFollowupDate" id="taskFollowupDate" class="taskFollowupDate form-control" required>

                                    </div>

                                    <!-- Modal buttons (Save and close) -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                        <button type="submit" name="followUpTask" class="btn btn-primary">Taak opvolgen</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>

                <!-- row area start -->
                <div class="row-fluid">
                <br>
                        <!-- Accordeon menu -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title noselect" style="color:#08475b"><i class="ti-actions"></i>&nbsp;&nbsp;Taken aanmaken</h4>
                                    <!-- Start accordion menu -->
                                    <div id="accordion3" class="according accordion-s3">
                                        <!-- SALES ACCORDEON -->
                                        <?=$S4Portal->renderNewTaskSalesTab();?>
                                        <!-- SUPPORT ACCORDEON -->
                                        <?=$S4Portal->renderNewTaskSupportTab();?>
                                        <!-- ADMINISTRATIE ACCORDEON -->
                                        <?=$S4Portal->renderNewTaskAdminTab();?>
                                    </div>
                                </div>
                            </div>
                        </div>

                <br>
                    <div class="card">
                        <div class="card-body">
                            <h4 style="color:#08475b" class="header-title"><i class="ti-actions"></i>&nbsp;Alle taken</h4>
                            <div id="accordion2" class="according accordion-s2">
                                <div class="card">
                                    <div class="card-header">
                                    </div>
                                        <div class="card-body">
                                            <div class="single-table">
                                                <div class="table-responsive">
                                                    <table id="tasks" class="table table-hover progress-table text-center">
                                                        <!-- Table headers -->
                                                        <thead class="text-uppercase">
                                                            <tr>
                                                                <th scope="col">Org</th>
                                                                <th scope="col">Omschrijving</th>
                                                                <th scope="col">Door</th>
                                                                <th scope="col">Actiehouder</th>
                                                                <th scope="col">Opvolgactie</th>
                                                                <th scope="col">Opvolgdatum</th>
                                                                <th scope="col">Contactpersoon</th>
                                                                <th scope="col">Tel</th>
                                                                <th scope="col">Acties</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?=$S4Portal->renderTakenTable()?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row area end -->
                <div class="row mt-5"></div>
            </div>
        </div>
        <!-- main content area end -->

        <!-- footer area start -->
        <?php $S4Portal->renderFooter(); ?>
        <!-- footer area end -->
    </div>

    <!-- Headers bottom -->
    <?php require "headersBottom.php"; ?>
</body>

</html>