<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}
/* If user is not logged in, redirect him back to login page */
if ($_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}
/* Initialise the class and assign variable to use it below */
require "classes/s4portal.class.php";
$S4Portal = new S4Portal();
/* Create notification ( if needed ) */
$S4Portal->createNotification();
/* Notification bar */
echo $S4Portal->getNotify();

$errorMsg = '<div style="text-align:center" class="alert alert-danger" role="alert"><strong>LET OP: Er zijn nog geen organisaties aangemaakt! - </strong><a href="nieuweorganisatie.php" class="alert-link">Klik hier</a> om een organisatie aan te maken alvorens nieuwe contactpersonen kunnen worden ingevoerd. </div>';
?>

<!doctype html>
<!-- no-js until js is attached to it -->
<div class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>S4Portal</title>
        <!-- Top headers -->
        <?php require "headers.php"; ?>
    </head>

    <body>
        <!-- preloader area start -->
        <div id="preloader">
            <div class="loader"></div>
        </div>
        <!-- preloader area end -->

        <!-- page container area start -->
        <div class="page-container">

            <!-- sidebar menu area start -->
            <?php $S4Portal->renderMenu(); ?>
            <!-- sidebar menu area end -->

            <!-- main content area start -->
            <div class="main-content">

                <!-- header area start -->
                <div class="header-area">
                    <div class="row align-items-center">

                        <!-- nav and search button start -->
                        <?php $S4Portal->renderNavAndSearch(); ?>
                        <!-- nav and search button end -->

                        <!-- profile info & task notification start -->
                        <?php $S4Portal->renderProfileAndNotifications(); ?>
                        <!-- profile info & task notification end -->

                    </div>
                </div>
                <!-- header area end -->

                <!-- page title area start -->
                <?php $S4Portal->renderPageTitle("Nieuwe contactpersoon aanmaken"); ?>
                <!-- page title area end -->

                <div class="main-content-inner">

                    <!-- row area start -->
                    <div class="row-fluid">

                        <div class="col-lg-12">
                            <div class="row">

                                <!-- Organisatie -->
                                <!-- Left side of form -->
                                <div class="col-6 mt-3">
                                    <div class="card">
                                        <div class="card-body">

                                            <!-- Form title -->
                                            <h4 class="header-title noselect" style="color:#08475b"><i class="ti-contacts"></i>&nbsp;Contactpersoon aanmaken</h4>

                                            <!-- Start form -->
                                            <form method="POST" action="controller.php?newContact" autocomplete="off">

                                                <!-- Organisatie -->
                                                <div class="form-group">
                                                    <label for="organisation" class="col-form-label noselect">Organisatie</label>
                                                    <select class="form-control" id="sex" name="organisation" required>
                                                        <?= $S4Portal->getAllOrganisationOptions(); ?>
                                                    </select>
                                                </div>

                                                <!-- Geslacht -->
                                                <div class="form-group">
                                                    <label for="sex" class="col-form-label noselect">Geslacht</label>
                                                    <select class="form-control" id="sex" name="sex" required>
                                                        <option value="man">Man</option>
                                                        <option value="vrouw">Vrouw</option>
                                                    </select>
                                                </div>

                                                <div class="row">
                                                    <!-- Voornaam -->
                                                    <div class="form-group col-md-6">
                                                        <label for="firstname" class="col-form-label noselect">Voornaam</label>
                                                        <input name="firstname" class="form-control" type="text" value="" id="firstname" required>
                                                    </div>

                                                    <!-- Achternaam -->
                                                    <div class="form-group col-md-6">
                                                        <label for="lastname" class="col-form-label noselect">Achternaam</label>
                                                        <input name="lastname" class="form-control" type="text" value="" id="lastname" required>
                                                    </div>
                                                </div>
                                                <span style="display:inline-block;height:28px"></span>
                                        </div>
                                    </div>
                                </div>

                                <!-- right side of form -->
                                <div class="col-6 mt-3">
                                    <div class="card">
                                        <div class="card-body">

                                            <!-- Functie -->
                                            <div class="form-group">
                                                <label for="function" class="col-form-label noselect">Functie</label>
                                                <input name="function" class="form-control" type="text" value="" id="function" required>
                                            </div>

                                            <!-- Email -->
                                            <div class="form-group">
                                                <label for="email" class="col-form-label noselect">Emailadres</label>
                                                <input name="email" class="form-control" type="email" value="" id="email" required>
                                            </div>

                                            <div class="row">
                                                <!-- Telefoonnummer (vast) -->
                                                <div class="form-group col-md-6">
                                                    <label for="landline" class="col-form-label noselect">Telefoonnummer (vast)</label>
                                                    <input name="landline" type="text" class="form-control" id="landline" value="" required>
                                                </div>
                                                <!-- Telefoonnummer (mobiel) -->
                                                <div class="form-group col-md-6">
                                                    <label for="mobile" class="col-form-label noselect">Telefoonnummer (mobiel)</label>
                                                    <input name="mobile" type="text" class="form-control" id="mobile" value="" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Submit button -->
                                    <input class="btn btn-primary btn-lg btn-block" type="submit" value="Contactpersoon invoeren" name="submitNewContact" <?php if (!$S4Portal->orgExistence) {
                                                                                                                                                                echo "disabled";
                                                                                                                                                            }; ?>>
                                </div>

                                </form>
                                <!-- End form -->
                            </div>
                            <br>
                            <!-- Show error message if no organisations found -->
                            <?php if (!$S4Portal->orgExistence) {
                                echo $errorMsg;
                            }; ?>
                        </div>


                        <div class="col-md-12 mt-3">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title noselect" style="color:#08475b"><i class="ti-contacts"></i>&nbsp;Alle contactpersonen</h4>
                                    <div class="single-table">
                                        <div class="table-responsive">
                                            <table id="contacts" class="table table-hover progress-table text-center">
                                                <thead class="text-uppercase">
                                                    <tr>
                                                        <th scope="col">Org.</th>
                                                        <th scope="col">Geslacht</th>
                                                        <th scope="col">Voornaam</th>
                                                        <th scope="col">Achternaam</th>
                                                        <th scope="col">Functie</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Tel vast</th>
                                                        <th scope="col">Tel mobiel</th>
                                                        <th scope="col">Acties</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?= $S4Portal->renderContactpersonenTable(); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- main content area end -->

            <!-- footer area start-->
            <?php $S4Portal->renderFooter(); ?>
            <!-- footer area end-->
        </div>

        <!-- Bottom headers required on all pages -->
        <?php require "headersBottom.php"; ?>
</div>
</body>