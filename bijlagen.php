<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Start session if not already started */
if (!isset($_SESSION)) {
    session_start();
}

/* If user is not logged in, redirect him back to login page */
if ($_SESSION['loggedIn'] !== true) {
    header("Location: login.php");
}
/* Initialise the class and assign variable to use it below */
require "classes/s4portal.class.php";
$S4Portal = new S4Portal();
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>S4Portal</title>
    <?php require "headers.php"; ?>
    <script src="assets/js/jquery.form.js"></script>
</head>
<body>

    <!-- preloader area start -->
    <?php $S4Portal->renderPreloader(); ?>
    <!-- preloader area end -->

    <!-- page container area start -->
    <div class="page-container">

        <?php $S4Portal->renderMenu(); ?>

        <!-- main content area start -->
        <div class="main-content">

            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">

                    <!-- nav and search button start -->
                    <?php $S4Portal->renderNavAndSearch(); ?>
                    <!-- nav and search button end -->

                    <!-- profile info & task notification start -->
                    <?php $S4Portal->renderProfileAndNotifications(); ?>
                    <!-- profile info & task notification end -->
                </div>
            </div>
            <!-- header area end -->

            <!-- page title area start -->
            <?php $S4Portal->renderPageTitle("Bijlagen en documenten"); ?>
            <!-- page title area end -->

            <div class="main-content-inner">

                <!-- row area start -->
                <div class="row">

                    <div class="col-md-3 mt-3">

                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title noselect" style="color:#08475b"><i class="ti-file"></i>&nbsp;Bijlagen uploaden</h4>

                                <!-- Upload file -->
                                <form name="audio_form" id="audio_form" action="" method="post" enctype="multipart/form-data">

                                    <!-- Organisatie -->
                                    <div class="form-group">
                                        <label for="organisation" class="col-form-label noselect">Organisatie</label>
                                        <select class="form-control" id="organisation" name="organisation" required>
                                            <?=$S4Portal->getAllOrganisationOptions(); ?>
                                        </select>
                                    </div> 

                                    <!-- File input -->
                                    <!-- <label for="docfile" class="col-form-label noselect">Kies een bestand</label>
                                    <input class="form-control" type="file" name="docfile" id="docfile" accept=".pdf,.doc,.xlsx">
                                    <br> -->

                                    <div class="custom-file">
                                    <input class="form-control" type="file" name="docfile" id="docfile" accept=".pdf,.doc,.xlsx">
                                    </div>
                                    <br><br>

                                    <!-- upload button -->
                                    <button type="button" onclick="uploadFile()" class="btn btn-block btn-primary btn-lg" id="hide-btn">Kies eerst een bestand</button>
                                    <br>

                                </form>

                                <!-- second progress bar -->
                                <div class="progress_area">
                                    <div class="progress">
                                        <div id="uploadProg" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9 mt-3">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title noselect" style="color:#08475b"><i class="ti-file"></i>&nbsp;Alle bijlagen</h4>
                                <div class="single-table">
                                                <div class="table-responsive">
                                                    <table id="organisations" class="table table-hover progress-table text-center">
                                                        <thead class="text-uppercase">
                                                            <tr>
                                                                <th scope="col">Organisatie</th>
                                                                <th scope="col">Bestand</th>
                                                                <th scope="col">Grootte (MB)</th>
                                                                <!-- <th scope="col">Type</th> -->
                                                                <th scope="col">Datum</th>
                                                                <th scope="col">Acties</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?= $S4Portal->getAllBijlagen(); ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row area end -->

                <div class="row mt-5">

                </div>

            </div>
        </div>
        <!-- main content area end -->

        <!-- footer area start -->
        <?php $S4Portal->renderFooter(); ?>
        <!-- footer area end -->
    </div>

    <!-- Headers bottom -->
    <?php require "headersBottom.php"; ?>
</body>

</html>